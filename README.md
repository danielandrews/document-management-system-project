Simple document management system in Grails

Configuration:

Modify Config.groovy environment variable uploadFolder to a folder path of your choice. You will need to have write permissions on this folder.

Execution:

grails > run-app
