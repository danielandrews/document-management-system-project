package document.management.system.project

class Document {
	
	String fileName
	String fullPath
	Date uploadDate = new Date()

    static constraints = {
		fileName(blank:false,nullable:false)
		fullPath(blank:false,nullable:false)
    }
}
